voltaApp.service('loginService', function($http, $localStorage) {
  var self = this;
  // Check if user has login in App
  this.isLoggedIn = function() {
    console.log($localStorage.user);
    return $localStorage.user !== undefined;
  };

  this.logout = function() {
    $localStorage.$reset();
  }

  this.login = function(credentials, successCallback, errorCallback) {
    var config = {
      'headers': {
        'Authorization': 'Basic ' + btoa(credentials.userName + ':' + credentials.password)
      }
    };

    $http.get(API_1, config).then(function(response) {
      $localStorage.user = response.data;
      $localStorage.credentials = { userName: credentials.userName, password: credentials.password };
      self.prepareHeaders();
      successCallback();
    }, function() {
      errorCallback();
    });
  }

  this.prepareHeaders = function() {
    $http.defaults.headers.common['X-VoltaApp'] = 'volta';
    if ($localStorage.credentials) {
      $http.defaults.headers.common['Authorization'] = 'Basic ' + btoa($localStorage.credentials.userName + ':' + $localStorage.credentials.password)
    }
  }
});
