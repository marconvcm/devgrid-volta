voltaApp.service('applianceService', function($http, $localStorage) {
  this.getAll = function(callback){
    $http.get(API_2).then(function(response){
      $localStorage.appliances = response.data;
      callback($localStorage.appliances);
    }, function(){
      callback($localStorage.appliances);
    })
  };
});
