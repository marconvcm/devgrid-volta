voltaApp.service('profileService', function($http, $localStorage) {
  this.getProperty = function() {
    return JSON.parse(JSON.stringify($localStorage.user.property));
  }
  this.setProperty = function(property, callbackSuccess, callbackError) {
    $localStorage.user.property = property;
    $http.patch(API_4, $localStorage.user.property).then(function(){
      callbackSuccess();
    }, function(){
      callbackError();
    })
  }
});
