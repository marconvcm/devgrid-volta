voltaApp.controller('loginCtrl', function($scope, loginService) {

  $scope.init = function(){
    loginService.prepareHeaders();
    $scope.goToHome();
  }


  $scope.login = function() {
    loadingModal.show();
    loginService.login($scope, function() {
      loadingModal.hide();
      $scope.goToHome();
    }, function() {
      loadingModal.hide();

      $scope.userName = '';
      $scope.password = '';

      ons.notification.alert({
        message: 'Something went wrong with your login',
        title: 'Login',
        buttonLabel: 'OK',
        animation: 'default',
      });
    });
  }

  $scope.goToHome = function() {
    if (loginService.isLoggedIn()) {
      appNavigator.pushPage('views/home/index.html');
    }
  }
});
