voltaApp.controller('homeCtrl', function($http, $scope, applianceService) {

  $scope.isLiveChecked = "1";

  $scope.goToProfile = function() {
    appNavigator.pushPage('views/profile/index.html');
  }

  $scope.getFilter = function(){
    if($scope.isLiveChecked == "1"){
      return { live: true };
    }else{
      return {};
    }
  }

  $scope.init = function() {
    loadingModal.show();
    applianceService.getAll(function(result) {
      loadingModal.hide();
      $scope.appliances = result;
    });
  }
});
