voltaApp.controller('profileCtrl', function($scope, profileService, loginService) {
  $scope.model = profileService.getProperty();

  $scope.save = function() {

    loadingModal.show();
    profileService.setProperty($scope.model, function() {
      loadingModal.hide();
      ons.notification.alert({
        message: 'Data have been saved!',
        title: 'My house',
        buttonLabel: 'OK',
        animation: 'default',
      });
    }, function() {
      loadingModal.hide();
      ons.notification.alert({
        message: 'Something wrong happened!',
        title: 'My house',
        buttonLabel: 'OK',
        animation: 'default',
      });
    });
  }

  $scope.logout = function() {
    ons.notification.confirm({
      title: 'Logout',
      message: 'Are you sure you want to continue?',
      callback: function(idx) {
        switch (idx) {
          case 0:
            break;
          case 1:
            loginService.logout();
            appNavigator.resetToPage('views/login/index.html');
            break;
        }
      }
    });
  }
});
